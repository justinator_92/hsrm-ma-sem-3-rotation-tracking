package hsrm.cs.ma.poseestimation;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity  {

    private static String TAG = MainActivity.class.getName();

    private ImageButton photo_btn;
    private ListView listView;
    private LazyAdapter adapter;
    private ArrayList<File> images;

    static {
        if(!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV is loaded");
        } else {
            Log.d(TAG, "OpenCV loading failed..");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        photo_btn = (ImageButton) findViewById(R.id.photo_btn);

        photo_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            startActivity(new Intent(MainActivity.this, PhotoActivity.class));
            }
        });

        listView = (ListView)findViewById(R.id.list);

        images = readImageFiles();
        Collections.reverse(images);
        adapter = new LazyAdapter(this, images);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);

        // Click event for single list row
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String fileName = images.get(position).toString();
                if(fileName == null || fileName.isEmpty()) {
                    return;
                }

                Intent i = new Intent(MainActivity.this, VideoActivity.class);
                i.putExtra("fileName", fileName);
                startActivity(i);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.list) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.show_histo:
                showHistogram(info.position);
                return true;
            case R.id.delete:
                safeDeleteFile(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showHistogram(int position) {
        Intent i = new Intent(MainActivity.this, GraphActivity.class);
        String fileName = images.get(position).getAbsolutePath();
        Log.d(TAG, "show histogram: " + fileName);
        i.putExtra("fileName", fileName);
        startActivity(i);
    }

    private void safeDeleteFile(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogStyle);
        builder.setTitle("Delete File");
        builder.setMessage("Are you sure you want to delete this file?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFileFromDisk(position);
                images.remove(position);
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void deleteFileFromDisk(int position) {
        File toDelete = images.get(position);
        String fileName = toDelete.toString();
        Log.d(TAG, fileName);
        deleteFileByName(fileName);
        deleteFileByName(fileName.replace(".jpg", ".ser"));
    }

    private void deleteFileByName(String fileName) {
        File dir = getFilesDir();
        File[] fList = dir.listFiles();
        for(File file : fList) {
            if(file.isFile() && file.toString().equals(fileName)) {
                file.delete();
                return;
            }
        }
    }

    private ArrayList<File> readImageFiles() {
        images = new ArrayList<>();
        File dir = getFilesDir();
        File[] fList = dir.listFiles();
        for (File file : fList) {
            if (file.isFile() && file.toString().endsWith(".jpg")) {
                try {
                    images.add(file);
                }catch (Exception ex) {
                    Log.d(TAG, "Error while reading " + file);
                }
            }
        }
        return images;
    }

}
