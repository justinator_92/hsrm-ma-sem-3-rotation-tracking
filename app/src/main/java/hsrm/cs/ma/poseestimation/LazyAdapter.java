package hsrm.cs.ma.poseestimation;

/**
 * Created by Justin Albert on 13.02.17.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class LazyAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<File> data;
    private static LayoutInflater inflater = null;

    public LazyAdapter(Activity activity, ArrayList<File> data) {
        this.activity = activity;
        this.data = data;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView == null) {
            vi = inflater.inflate(R.layout.list_row, null);
        }

        File data = this.data.get(position);

        TextView title = (TextView)vi.findViewById(R.id.file_name);
        TextView creation_date = (TextView)vi.findViewById(R.id.creation_date);
        ImageView imageView = (ImageView)vi.findViewById(R.id.list_image);

        Drawable d = Drawable.createFromPath(data.toString());
        imageView.setImageDrawable(d);

        title.setText(data.getName());

        Date lastModDate = new Date(data.lastModified());
        creation_date.setText(lastModDate.toString());
        return vi;
    }
}