package hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.utils;

import android.util.Log;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Created by Justin Albert on 20.04.17.
 */
public class ImageLoader {

    public static String TAG = ImageLoader.class.getName();

    public static int[] loadReferenceImage(String fileName) {
        int[] ret = null;
        fileName = fileName.replace(".jpg", ".ser");  // change file extension

        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream(fileName));
            ret = (int[]) in.readObject();
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        } finally {
            try {
                in.close();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }
        return ret;
    }

}
