package hsrm.cs.ma.poseestimation;

/**
 * Created by Justin Albert on 13.02.17.
 */

public enum CameraMode {

    /**
     * display camera image in binary format
     */
    BINARY,

    /**
     * display camera image in grayvalues
     */
    GRAYVALUES,


    /**
     * display image with colors, rgb
     */
    COLOR

}
