package hsrm.cs.ma.poseestimation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;

import hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.math.ImageUtils;
import hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.utils.ImageLoader;

/**
 * Created by Justin Albert on 09.02.17.
 */
public class VideoActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    public final String TAG = VideoActivity.class.getName();

    public int ImageWidth = 1920;
    public int ImageHeight = 1080;
    public final double THRESHOLD = 0.5;

    private CameraMode cameraMode = CameraMode.BINARY;

    public final int RETRYMAX = 250; // retry max for 250 frames
    private int retries;
    public int lastAngle;
    public int rotationAngle = 0;

    private AdvancedCameraView javaCameraView;
    private Mat mRgba, imgGray, imgBinary;

    private byte[] buffer;
    private Point center;

    private int degrees = 5;
    private int totaldegrees = 360 / degrees;
    private Size size = new Size(3,3);

    public int[] values;
    public int[] referenceArray;
    public double[] referenceValues;
    public double[] valuesArray;

    private MenuItem[] mEffectMenuItems;
    private SubMenu mColorEffectsMenu;

    BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case BaseLoaderCallback.SUCCESS: {
                    javaCameraView.enableView();
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
            super.onManagerConnected(status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // load reference image...
        Bundle b = getIntent().getExtras();
        String fileName = b.getString("fileName");
        referenceArray = ImageLoader.loadReferenceImage(fileName);
        if (referenceArray == null) {
            Log.d(TAG, "couldn't load reference image: " + fileName);
            finish();
        } else {
            referenceValues = ImageUtils.normalizeGraph(referenceArray);
            degrees = 360 / referenceValues.length;
            totaldegrees = referenceValues.length;
        }

        // start android camera view
        javaCameraView = (AdvancedCameraView) findViewById(R.id.java_camera_view);
        javaCameraView.enableFpsMeter();
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);

        buffer = new byte[ImageWidth * ImageHeight];
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mColorEffectsMenu = menu.addSubMenu("Colors");
        mEffectMenuItems = new MenuItem[3];

        int idx = 0;
        for(CameraMode mode : CameraMode.values()) {
            String element = mode.toString();
            mEffectMenuItems[idx] = mColorEffectsMenu.add(1, idx, Menu.NONE, element);
            idx++;
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getGroupId() == 1)
        {
            cameraMode = CameraMode.valueOf((String)item.getTitle());
            Toast.makeText(this, cameraMode.toString(), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV is loaded");
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        } else {
            Log.d(TAG, "OpenCV failed..");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallBack);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        imgGray = new Mat(height, width, CvType.CV_8UC1);
        imgBinary = new Mat(height, width, CvType.CV_8UC1);
        buffer = new byte[ImageWidth * ImageHeight];
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        imgGray.release();
        imgBinary.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat ret = null;
        mRgba = inputFrame.rgba();
        imgGray = inputFrame.gray();
        // image analysis operations
        Imgproc.GaussianBlur(imgGray, imgBinary, size, 3);
        Imgproc.threshold(imgBinary, imgBinary, 0, 255, Imgproc.THRESH_BINARY |Imgproc.THRESH_OTSU);

        // calculate center point and rotation
        imgBinary.get(0, 0, buffer);
        center = ImageUtils.getCenterPoint(buffer, ImageWidth, ImageHeight);
        Imgproc.circle(imgBinary, center, 5, new Scalar(255));

        values = ImageUtils.calculateRotationGraph(buffer, center, degrees, totaldegrees);
        valuesArray = ImageUtils.normalizeGraph(values);

        switch (cameraMode) {
            case BINARY: {
                ret = imgBinary;
                break;
            }
            case GRAYVALUES: {
                ret = imgGray;
                break;
            }
            case COLOR: {
                ret = mRgba;
                break;
            }
        }

        lastAngle = ImageUtils.calculateRotationAngle(valuesArray, referenceValues, THRESHOLD, degrees, totaldegrees);
        Log.d(TAG, "rotation: " + lastAngle);

        if (lastAngle >= 0) {
            rotationAngle = lastAngle;
            retries = 0; // reset retries
        } else {
            retries += 1;
        }

        if (retries > 50) {
            Imgproc.putText(ret, "Couldn't track object", new Point(1450, 1050), 3, 1, new Scalar(0), 2);
        }

        if (retries < RETRYMAX) {
            Point rot = ImageUtils.rotateVector(new Point(1, 0), -rotationAngle);
            // angle line
            Imgproc.line(ret, new Point(center.x, center.y),
                              new Point(rot.x * 100 + center.x,
                                        rot.y * 100 + center.y),
                              new Scalar(255));
            // reference line
            Imgproc.line(ret, new Point(center.x, center.y),
                              new Point(center.x + 100,
                                        center.y),
                              new Scalar(255));
        }  else {
            rotationAngle = -1;
        }
        return ret;
    }
}
