package hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.math;

/**
 * Created by Justin Albert on 17.04.17.
 */
public class Rounding {

    public static int toInt32(double value) {
        return (int)(value + 0.5);
    }

    public static int getMaximum(int[] values) {
        int max = Integer.MIN_VALUE;
        int value;
        for (int i = 0; i < values.length; i += 1) {
            value = values[i];
            if (value > max) {
                max = value;
            }
        }
        return max;
    }

}
