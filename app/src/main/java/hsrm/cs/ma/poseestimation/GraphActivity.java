package hsrm.cs.ma.poseestimation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.math.Rounding;
import hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.utils.ImageLoader;

/**
 * Created by Justin Albert on 20.04.17.
 */
public class GraphActivity extends AppCompatActivity {

    public String TAG = GraphActivity.class.getName();
    private GraphView graph;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        graph = (GraphView) findViewById(R.id.graph);
        graph.setTitle("Rotation Distances");

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);

        GridLabelRenderer gridLabel = graph.getGridLabelRenderer();
        gridLabel.setHorizontalAxisTitle("Degrees");
        gridLabel.setVerticalAxisTitle("Pixels");

        Bundle b = getIntent().getExtras();
        String fileName = b.getString("fileName");
        loadImageFile(fileName);
    }

    private void loadImageFile(String fileName) {
        int[] values = ImageLoader.loadReferenceImage(fileName);
        if (values == null) {
            return;
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        int scale = 360 / values.length;
        for (int i = 0; i < values.length; i += 1) {
            series.appendData(new DataPoint(i * scale, values[i]), false, 360, false);
        }
        graph.addSeries(series);

        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(360);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(Rounding.getMaximum(values) + 100);
    }

}
