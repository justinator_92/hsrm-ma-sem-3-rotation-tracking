package hsrm.cs.ma.poseestimation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.math.ImageUtils;

public class PhotoActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    protected final String TAG = PhotoActivity.class.getName();

    private AdvancedCameraView javaCameraView;

    public int ImageWidth = 1920;
    public int ImageHeight = 1080;

    private Mat imgGray, imgBinary;
    private byte[] buffer;
    private int[] rotationValues;
    private Point center;

    public final int Degrees = 2;
    public final int NumberDegrees = 360 / Degrees;

    public static Size size;

    private ImageButton capture_photo;

    BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case BaseLoaderCallback.SUCCESS: {
                    javaCameraView.enableView();
                    break;
                }

                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
            super.onManagerConnected(status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        javaCameraView = (AdvancedCameraView) findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);

        capture_photo = (ImageButton) findViewById(R.id.capture_photo);
        capture_photo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                capturePhoto();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String[] Modes = {"OTSU", "BINARY 50", "BINARY 100", "BINARY 127", "BINARY 150", "BINARY 200"};

        SubMenu mBinarisationModeMenu = menu.addSubMenu("Binarisation");
        MenuItem[] mBinarisationMenuItems = new MenuItem[Modes.length];

        int idx = 0;
        for (String mode : Modes) {
            mBinarisationMenuItems[idx] = mBinarisationModeMenu.add(1, idx, Menu.NONE, mode);
            idx++;
        }

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV is loaded");
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);

        } else {
            Log.d(TAG, "OpenCV sucks..");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallBack);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        imgGray = new Mat(height, width, CvType.CV_8UC1);
        imgBinary = new Mat(height, width, CvType.CV_8UC1);
        buffer = new byte[ImageWidth * ImageHeight];
    }

    @Override
    public void onCameraViewStopped() {
        imgGray.release();
        imgBinary.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        imgGray = inputFrame.gray();
        size = new Size(5,5);

        // do image analysis operations
        Imgproc.GaussianBlur(imgGray, imgBinary, size, 3);
        Imgproc.threshold(imgBinary, imgBinary, 0, 255, Imgproc.THRESH_BINARY |Imgproc.THRESH_OTSU);

        // calculate center point
        imgBinary.get(0, 0, buffer);
        center = ImageUtils.getCenterPoint(buffer, ImageWidth, ImageHeight);
        Imgproc.circle(imgBinary, center, 5, new Scalar(255));
        Log.d(TAG, "Center: " + center.x + "," + center.y);

        rotationValues = ImageUtils.calculateRotationGraph(buffer, center, Degrees, NumberDegrees);
        Log.d(TAG, ImageUtils.PrintArray(rotationValues));

        return imgBinary;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item.getGroupId() == 1)
        {
            String s = (String) item.getTitle();
            String[] split = s.split(" ");
/*
            if(split[0].equals("OTSU")) {
                BinaryMode = Imgproc.THRESH_OTSU;
                BinaryThreshold = 0;
            } else if (split[0].equals("BINARY")) {
                BinaryMode = Imgproc.THRESH_BINARY;
                try {
                    BinaryThreshold = Integer.parseInt(split[1]);
                } catch (Exception ex) {
                    BinaryThreshold = 127;
                }
            }*/

            Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    public void capturePhoto() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        File dir = getFilesDir();
        String fileNameNoExtension = dir.getAbsolutePath() + "/OBJ_" + currentDateandTime + ".jpg";
        javaCameraView.takePicture(fileNameNoExtension);

        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(dir.getAbsolutePath() + "/OBJ_" + currentDateandTime + ".ser"));
            out.writeObject(rotationValues);
            out.flush();
            out.close();
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
        finish();
    }

}