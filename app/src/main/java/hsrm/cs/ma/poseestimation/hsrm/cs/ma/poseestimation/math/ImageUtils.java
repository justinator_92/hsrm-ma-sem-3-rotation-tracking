package hsrm.cs.ma.poseestimation.hsrm.cs.ma.poseestimation.math;

import android.util.Log;

import org.opencv.core.Point;

/**
 * Created by Justin Albert on 17.04.17.
 */
public class ImageUtils {

    public static String TAG = ImageUtils.class.toString();

    public static final int SmallFrameWidth = 1920;
    public static final int SmallFrameHeight = 1080;

    public static int[] calculateRotationGraph(byte[] data, Point center, int degrees, int nrDegrees) {
        int[] src = new int[nrDegrees];
        Point direction = new Point(1, 0);
        Point rot;
        for(int i = 0; i < nrDegrees; i += 1) {
            rot = rotateVector(direction, i * degrees);
            src[i] = getDistanceFromCenterPoint(data, rot, center);
        }
        return src;
    }

    public static Point rotateVector(Point dir, double angle) {
        double cosAlpha = Math.cos(angle * Math.PI / 180.0);
        double sinAlpha = Math.sin(angle * Math.PI / 180.0);

        return new Point(cosAlpha * dir.x - sinAlpha * dir.y,
                         sinAlpha * dir.x + cosAlpha * dir.y);
    }

    private static int getDistanceFromCenterPoint(byte[] data, Point dir, Point center) {
        double x = center.x;
        double y = center.y;
        double dX, dY;

        while (x > 0 && x < SmallFrameWidth && y > 0 && y < SmallFrameHeight) {
            if(data[(int)y * SmallFrameWidth + (int)x] == -1) {  // border detected...
                break;
            }
            x += dir.x;
            y += dir.y;
        }
        dX = x - center.x;
        dY = y - center.y;
        return Rounding.toInt32(Math.sqrt(dX * dX + dY * dY));
    }

    public static int calculateRotationAngle(double[] distances, double[] referenceDistances, double threshold, int degrees, int nrDegrees) {
        double a1, a2, dist;
        for (int shift = 0; shift < nrDegrees; shift += 1) {
            boolean result = true;

            for (int count = 0; count < nrDegrees; count += 1) {
                a1 = referenceDistances[(shift + count) % nrDegrees];
                a2 = distances[count];
                dist = Math.abs(a1 - a2);
                if (dist < -threshold || dist > threshold) {
                    result = false; break;
                }
            }
            if (result) {
                return shift * degrees;
            }
        }
        return -1;
    }

    public static double[] normalizeGraph(int[] values) {
        double[] d_values = new double[values.length];
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        int value;
        for (int i = 0; i < values.length; i += 1) {
            value = values[i];
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }

        double fraction = (max - (double) min);
        for (int i = 0; i < values.length; i += 1) {
            d_values[i] = (values[i] - min) / fraction;
        }
        return d_values;
    }

    public static Point getCenterPoint(byte[] buffer, int smallFrameWidth, int smallFrameHeight) {
        int sumX = 0, sumY = 0, count = 0;
        for(int x = 0; x < smallFrameWidth; x += 1) {
            for(int y = 0; y < smallFrameHeight; y += 1) {
                if(buffer[y * smallFrameWidth + x] == 0) {  // if object is white
                    sumX += x;
                    sumY += y;
                    count += 1;
                }
            }
        }
        if (count == 0) {
            return new Point(-1, -1);
        }
        return new Point(sumX / (double) count, sumY / (double) count);
    }

    public static String PrintArray(int[] array) {
        String s = "";
        for (int i = 0; i < array.length; i += 1) {
            s += array[i] + ",";
        }
        return s;
    }

    public static String PrintArray(double[] array) {
        String s = "";
        for(int i = 0; i < array.length; i += 1) {
            s += array[i] + ",";
        }
        return s;
    }

}
